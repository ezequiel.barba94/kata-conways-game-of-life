package oe.barbero.kata.conwaysgameoflife.core

import oe.barbero.kata.conwaysgameoflife.actions.GameFacade
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class EcosystemTests {
    @Test
    fun `an ecosystem without living cells has no life after a generation`() {
        val initialEcosystem = Ecosystem.createLifeless()

        initialEcosystem.nextGeneration()

        assertThat(facade.isLifeless).isTrue()
    }

    @Test
    fun `an ecosystem with only one living cell is lifeless after a generation`() {
        facade.addLiving(3, 3)

        facade.nextGeneration()

        assertThat(facade.isLifeless).isTrue()
    }

    @Test
    fun `a cell with two neighbours lives after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(3, 3)
        facade.addLiving(4, 3)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(3, 3)).isTrue()
        assertThat(facade.isLifeless).isFalse()
    }

    @Test
    fun `a cell with three neighbours lives after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(3, 3)
        facade.addLiving(4, 3)
        facade.addLiving(3, 2)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(3, 3)).isTrue()
        assertThat(facade.isLifeless).isFalse()
    }

    @Test
    fun `a cell with a neighbour dies after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(3, 3)
        facade.addLiving(4, 3)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(2, 3)).isFalse()
        assertThat(facade.isLifeless).isFalse()
    }

    @Test
    fun `a cell with more than three neighbours dies after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(3, 3)
        facade.addLiving(4, 3)
        facade.addLiving(3, 2)
        facade.addLiving(3, 4)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(3, 3)).isFalse()
        assertThat(facade.isLifeless).isFalse()
    }

    @Test
    fun `a dead cell with exactly three neighbours comes to life after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(3, 2)
        facade.addLiving(4, 3)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(3, 3)).isTrue()
        assertThat(facade.isLifeless).isFalse()
    }

    @Test
    fun `a dead cell with two neighbours stays dead after a generation`() {
        facade.addLiving(2, 3)
        facade.addLiving(4, 3)

        facade.nextGeneration()

        assertThat(facade.hasLifeAt(3, 3)).isFalse()
        assertThat(facade.isLifeless).isTrue()
    }

    private lateinit var facade: GameFacade

    @BeforeEach
    fun beforeEach() {
        facade = GameFacade(Ecosystem.createLifeless())
    }
}
