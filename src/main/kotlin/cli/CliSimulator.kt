package oe.barbero.kata.conwaysgameoflife.cli

import oe.barbero.kata.conwaysgameoflife.actions.GameFacade
import oe.barbero.kata.conwaysgameoflife.core.Ecosystem

class CliSimulator private constructor(private val facade: GameFacade, initial: String) {
    private val lines = initial.split('\n')
    private var height = lines.size
    private var width = lines[0].length

    private fun initialize() {
        for ((x, y) in cellCoordinates())
            facade.addLiving(x, y)
    }

    private fun cellCoordinates() = sequence {
        for ((y, line) in lines.withIndex())
            for (x in cellPositionOnLine(line))
                yield(Pair(x, y))
    }

    private fun cellPositionOnLine(line: String): MutableList<Int> {
        return line.foldIndexed(mutableListOf()) { index, cellPosition, char ->
            if (char == '#') cellPosition += index
            cellPosition
        }
    }

    fun generate(): String {
        facade.nextGeneration()
        return diagram()
    }

    private fun diagram() =
        (0 until height - 1).fold("") { text, y ->
            text + line(y) + '\n'
        } + line(height - 1)

    private fun line(y: Int): String =
        (0 until width).fold("") { line, x ->
            line + cell(x, y)
        }

    private fun cell(x: Int, y: Int) = if (facade.hasLifeAt(x, y)) "#" else "."

    companion object {
        fun fromPattern(pattern: String): CliSimulator {
            val cli = CliSimulator(GameFacade(Ecosystem.createLifeless()), pattern)
            cli.initialize()
            return cli
        }
    }
}