package oe.barbero.kata.conwaysgameoflife.actions

import oe.barbero.kata.conwaysgameoflife.core.Ecosystem
import oe.barbero.kata.conwaysgameoflife.core.LivingCell
import oe.barbero.kata.conwaysgameoflife.core.Location

class GameFacade(private var ecosystem: Ecosystem) {
    fun addLiving(x: Int, y: Int) {
        val location = Location(x, y)
        ecosystem.addCell(LivingCell(location))
    }

    fun hasLifeAt(x: Int, y: Int): Boolean {
        return ecosystem.isAliveAt(Location(x, y))
    }

    fun nextGeneration() {
        ecosystem = ecosystem.nextGeneration()
    }

    val isLifeless get() = ecosystem.isLifeless
}