package oe.barbero.kata.conwaysgameoflife.core

class DeadCell(location: Location) : Cell(location ) {
    override fun isAliveOnNextGeneration(numberOfNeighbours: Int) = FERTILE_NEIGHBOURHOOD == numberOfNeighbours

    companion object {
        private const val FERTILE_NEIGHBOURHOOD = 3
    }
}