package oe.barbero.kata.conwaysgameoflife.core

abstract class Cell(val location: Location) {
    abstract fun isAliveOnNextGeneration(numberOfNeighbours: Int): Boolean
}