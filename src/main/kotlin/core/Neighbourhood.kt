package oe.barbero.kata.conwaysgameoflife.core

class Neighbourhood : Sequence<Cell> {
    private val cellsPlacement = mutableMapOf<Location, Cell>()

    fun isEmpty() = cellsPlacement.isEmpty()

    operator fun plusAssign(cell: Cell) {
        if (cell is LivingCell)
            cellsPlacement[cell.location] = cell
    }

    override fun iterator() = iterator {
        for ((loc, cell) in cellsPlacement) {
            yield(cell)
            yieldAll(findNeighbours(loc))
        }
    }

    private fun findNeighbours(location: Location) = sequence {
        for (loc in location.adjacentLocations())
            yield(if (isAliveAt(loc)) LivingCell(loc) else DeadCell(loc))
    }

    fun numberOfNeighbours(cell: Cell) = cell.location.adjacentLocations().count { isAliveAt(it) }

    fun isAliveAt(location: Location) = cellsPlacement.containsKey(location)
}