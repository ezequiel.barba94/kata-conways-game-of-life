package oe.barbero.kata.conwaysgameoflife.core

class Ecosystem private constructor() {
    private val neighbourhood = Neighbourhood()
    val isLifeless get() = neighbourhood.isEmpty()

    fun addCell(cell: Cell) {
        neighbourhood += cell
    }

    fun isAliveAt(location: Location) = neighbourhood.isAliveAt(location)

    fun nextGeneration(): Ecosystem {
        val nextEcosystem = createLifeless()
        for (cell in neighbourhood)
            nextEcosystem.addCell(nextGenerationCell(cell))
        return nextEcosystem
    }

    private fun nextGenerationCell(cell: Cell): Cell {
        val numberOfNeighbours = neighbourhood.numberOfNeighbours(cell)
        return if (cell.isAliveOnNextGeneration(numberOfNeighbours))
            LivingCell(cell.location) else DeadCell(cell.location)
    }

    companion object {
        fun createLifeless() = Ecosystem()
    }
}
