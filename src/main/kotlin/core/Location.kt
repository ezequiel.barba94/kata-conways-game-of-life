package oe.barbero.kata.conwaysgameoflife.core

data class Location(private val x: Int, private val y: Int) {
    fun adjacentLocations() = sequence {
        yield(leftUp())
        yield(left())
        yield(leftDown())
        yield(down())
        yield(downRight())
        yield(right())
        yield(rightUp())
        yield(up())
    }

    private fun leftUp() = Location(x - 1, y + 1)
    private fun left() = Location(x - 1, y)
    private fun leftDown() = Location(x - 1, y - 1)
    private fun down() = Location(x, y - 1)
    private fun downRight() = Location(x + 1, y - 1)
    private fun right() = Location(x + 1, y)
    private fun rightUp() = Location(x + 1, y + 1)
    private fun up() = Location(x, y + 1)
}