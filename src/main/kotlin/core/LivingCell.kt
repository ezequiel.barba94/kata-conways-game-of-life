package oe.barbero.kata.conwaysgameoflife.core

class LivingCell(location: Location) : Cell(location) {
    override fun isAliveOnNextGeneration(numberOfNeighbours: Int): Boolean {
        return numberOfNeighbours == 2 || numberOfNeighbours == 3
    }
}
